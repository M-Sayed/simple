class User < ApplicationRecord
  validates :first_name, :last_name, :email, presence: true
  validates :email, uniqueness: true

  before_save :set_full_name

  private

  def set_full_name
    self.name = "#{first_name} #{last_name}"
  end
end
